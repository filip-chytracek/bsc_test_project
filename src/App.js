import React, { Component } from 'react';
import Notes from './components/endpoints/Notes';
import Form from './components/endpoints/Form';
import { Route } from 'react-router-dom';
import Menu from './components/Menu';
import { IntlProvider, addLocaleData } from 'react-intl';
import cs from './languages/cs.json';
import csLocaleData from 'react-intl/locale-data/cs';

addLocaleData(csLocaleData);

const text = {
	cs
};

class App extends Component {
	state = {
		language: `en`
	};

	changeLanguage = (language) => {
		this.setState({
			language
		});
	};

	render() {
		const { language } = this.state;

		return (
			<IntlProvider locale={language} messages={text[language]}>
				<React.Fragment>
					<header className="App-header">
						<Menu changeLanguage={this.changeLanguage} />
					</header>

					<Route path="/" exact component={Notes} />
					<Route path="/edit/:id" exact component={Form} />
					<Route path="/new" exact component={Form} />
				</React.Fragment>
			</IntlProvider>
		);
	}
}

export default App;
