import axios from 'axios';

const rest = axios.create({
	baseURL: 'http://private-9aad-note10.apiary-mock.com/'
});

const deleteNote = (id) => {
	return rest.delete(`/notes/${id}`);
};

const getNotes = () => {
	return rest.get(`/notes`);
};
const getNote = (id) => {
	return rest.get(`/notes/${id}`);
};

const createNote = (id, note) => {
	return rest.post(`/notes`, note);
};

const updateNote = (id, note) => {
	return rest.put(`/notes/${id}`, note);
};

const apiFunctions = {
	getNotes,
	createNote,
	updateNote,
	deleteNote,
	getNote
};

export default apiFunctions;
