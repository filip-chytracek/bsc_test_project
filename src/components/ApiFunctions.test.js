/***************************
 * Api TEST, best would be to import ID from database as highest number for generating
 */
import apiFunctions from './ApiFunctions';

const id = Math.floor(Math.random() * 2 + 1);
describe(`Api TEST`, () => {
	it('getNote', () => {
		return apiFunctions.getNote(id).then((result) => {
			expect(typeof result.data.id).toBe(`number`);
		});
	});
	it('deleteNote', () => {
		return apiFunctions.deleteNote(id).then((result) => {
			expect(result.config.method).toBe('delete');
		});
	});
	it('updateNote', () => {
		return apiFunctions.updateNote(id, 'testing').then((result) => {
			console.log(JSON.stringify(result.config, 0, 4));
			expect(result.data.id).toBe(id);
		});
	});
});
