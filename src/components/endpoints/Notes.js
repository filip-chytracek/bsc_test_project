import React, { Component } from 'react';
import apiFunctions from '../ApiFunctions';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import '../../index.css';

export default class Notes extends Component {
	state = {
		notes: null
	};

	deleteTitle = (titleId) => {
		apiFunctions
			.deleteNote(titleId)
			.then((result) => {
				console.log(`Note ${titleId} deleted.`);
				this.loadNotes();
			})
			.catch((error) => {
				console.error(error);
			});
	};

	loadNotes = () => {
		apiFunctions
			.getNotes()
			.then((result) => {
				this.setState({
					notes: result.data
				});
			})
			.catch((error) => {
				console.error(error);
			});
	};

	getRows = () => {
		if (this.state.notes === null) {
			return (
				<tr>
					<td colSpan="3">
						<FormattedMessage id="loading_data" defaultMessage="Loading data" />
					</td>
				</tr>
			);
		} else if (this.state.notes.length === 0) {
			return (
				<tr>
					<td colSpan="3">
						<FormattedMessage id="error_notes" defaultMessage="error_notes" />
					</td>
				</tr>
			);
		} else {
			return this.state.notes.map((note) => {
				return (
					<tr className={note.id % 2 === 0 ? 'NoteDrop' : null} key={note.id}>
						<td>{note.id}</td>
						<td>{note.title}</td>
						<td>
							<div className="actionButtons" onClick={(e) => this.deleteTitle(note.id)}>
								<FormattedMessage id="delete_note" defaultMessage="Delete" />
							</div>
							<Link className="actionButtons" to={`/edit/${note.id}`}>
								<FormattedMessage id="edit_note" defaultMessage="List" />
							</Link>
						</td>
					</tr>
				);
			});
		}
	};

	render() {
		return (
			<div>
				<table className="table">
					<thead className="thead-dark">
						<tr style={{ backgroundColor: 'darkgray' }}>
							<th>ID</th>
							<th>
								<FormattedMessage id="tab_title" defaultMessage="Title" />
							</th>
							<th>
								<FormattedMessage id="tab_action" defaultMessage="Action" />
							</th>
						</tr>
					</thead>
					<tbody>{this.getRows()}</tbody>
				</table>
			</div>
		);
	}

	componentDidMount() {
		this.loadNotes();
	}
}
