import React, { Component } from 'react';
import '../../index.css';
import apiFunctions from '../ApiFunctions';
import { FormattedMessage } from 'react-intl';

export default class Form extends Component {
	state = {
		title: ''
	};

	constructor(props) {
		super(props);
		this.mode = props.match.path === `/new` ? `new` : `editing`;
	}

	buttonSubmit = () => {
		if (this.mode === `editing`) {
			apiFunctions.updateNote(this.props.match.params.id, this.state.title).then((result) => {
				console.log(`Note ID: ${this.props.match.params.id} updated with text: "${this.state.title}".`);
				this.props.history.push(`/`);
			});
		} else if (this.mode === `new`) {
			apiFunctions
				.createNote(this.state.title)
				.then((result) => {
					console.log(`New note created with text: "${this.state.title}".`);
				})
				.catch((error) => {
					console.log(error);
				});
		} else {
			console.log(`Mode doesn't exist`);
		}
	};

	changeTitle = (e) => {
		this.setState({
			title: e.currentTarget.value
		});
	};

	render() {
		const status =
			this.mode === `new` ? (
				<FormattedMessage id="creating_note" defaultMessage="Creating a new note" />
			) : (
				<FormattedMessage
					id="editing_note"
					defaultMessage="Editing note with ID: {id}"
					values={{ id: this.props.match.params.id }}
				/>
			);
		return (
			<React.Fragment>
				<form style={{ margin: 50 }} onSubmit={(e) => e.preventDefault()}>
					<label>{status}</label>
					<input
						className="form-control"
						type="text"
						placeholder="Insert title"
						id="title"
						style={{ width: '600px' }}
						onChange={this.changeTitle}
					/>
					<button type="Submit" className="btn btn-primary" onClick={this.buttonSubmit}>
						<FormattedMessage id="button_submit" defaultMessage="Submit" descrpition="Submit to database" />
					</button>
				</form>
			</React.Fragment>
		);
	}
}
