import React from 'react';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

export default ({ changeLanguage }) => {
	return (
		<nav className="navbar navbar-default">
			<div className="container-fluid">
				<div className="navbar-header">
					<span className="navbar-brand">BSC list</span>
				</div>

				<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul className="nav navbar-nav">
						<li>
							<Link to="/" className="">
								<FormattedMessage id="list_menu" defaultMessage="List" />
							</Link>
						</li>
						<li>
							<Link to="/new">
								<FormattedMessage id="new_menu" defaultMessage="New" />
							</Link>
						</li>
					</ul>

					<span onClick={() => changeLanguage(`cs`)} className="language">
						CZ
					</span>

					<span onClick={() => changeLanguage(`en`)} className="language">
						EN
					</span>
				</div>
			</div>
		</nav>
	);
};
