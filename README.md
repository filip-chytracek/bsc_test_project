# BSC

#### Techhnologies

  - ReactJS
  - Rest API 

#### First start of server
```sh
$ npm install
$ npm run start
```
#### Testing
```sh
$ npm run test
```
##### Rest API documentation:
  - ###### createNote
    - creates new note with text from form 
  - ###### getNote
    -  returns single note specified by ID
  - ###### getNotes
    - returns all notes
  - ###### updateNote
    - updates note specified by ID and text from form     
  - ###### deleteNote
    - deletes note specified by ID 


